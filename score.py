import sys
import math
def Name(s):
    return s.split("#")[0].split("@")[0]
numOfDomains = len(sys.argv)-1;
for d in range(numOfDomains):
    name = sys.argv[d+1];
    numOfParties = 3;
    res = open(name, "rt");
    strs = res.readlines();
    sep = ";";
    seb = ",";
    if(d == 0):
        agents = []
        for row in range(2, len(strs)):
            if("failure" in strs[row]):
                continue
            if(sep not in strs[row]):
                sep = seb;
            tmp = (strs[row].split(sep))[12:12+numOfParties];
            for i in range(len(tmp)):
                if(Name(tmp[i]) not in agents):
                    agents.append(Name(tmp[i]));
        print(agents)
        numOfAgents = len(agents)
        util = [[] for i in range(numOfAgents)];
        numOfTerm = [0 for i in range(numOfAgents)];
        avg_util = [0 for i in range(numOfAgents)];
        std_util = [0 for i in range(numOfAgents)];
        count = [0 for i in range(numOfAgents)];
        time = [0 for i in range(numOfAgents)];
        social = [0 for i in range(numOfAgents)];
        nash = [0 for i in range(numOfAgents)];
        pareto = [0 for i in range(numOfAgents)];
        diff_percent = [0 for i in range(numOfAgents)];
    for s in strs[2:]:
        if("failure" in s):
            continue
        terms = s.split(sep);
        ags = []
        for i in range(numOfParties):
            for j in range(numOfAgents):
                if(Name(terms[12+i])==agents[j]):
                    if(len(terms[4])>0 and "No" in terms[4]):numOfTerm[j]+=1;
                    if(len(terms[0])>0):time[j]+=float(terms[0]);
                    if(len(terms[11])>0):social[j]+=float(terms[11]);
                    if(len(terms[10])>0):nash[j]+=float(terms[10]);
                    if(len(terms[9])>0):pareto[j]+=float(terms[9]);
                    count[j]+=1;
                    u = float(terms[12+numOfParties+i]);
                    util[j].append(u);
                    ags.append(j);
        max_util = max([util[j][-1] for j in ags])
        if(max_util == 0):
            continue
        for j in ags:
            diff_percent[j] += (util[j][-1]-max_util)/max_util;
for j in range(numOfAgents):
    for i in range(len(util[j])):
        avg_util[j]+=util[j][i]
for j in range(numOfAgents):
    avg_util[j]/=count[j]
for j in range(numOfAgents):
    for i in range(len(util[j])):
        std_util[j]+=(util[j][i]-avg_util[j])*(util[j][i]-avg_util[j])
for j in range(numOfAgents):
    std_util[j]=math.sqrt(std_util[j]/count[j])
    diff_percent[j]/=count[j]
    time[j]/=count[j];
    social[j]/=count[j];
    nash[j]/=count[j];
    pareto[j]/=count[j];
for j in range(numOfAgents):
    print(Name(agents[j]));
    print("\tNum. tournaments =", count[j]);
    print("\tNum. terminations =", numOfTerm[j])
    print("\tAvg. relative diff =", diff_percent[j]*100, "%");
    print("\tAvg. utility =", avg_util[j]);
    print("\tStd. utility =", std_util[j]);
    print("\tAvg. time =",time[j]);
    print("\tAvg. social welfare =",social[j]);
    print("\tAvg. nash =",nash[j]);
    print("\tAvg. pareto =",pareto[j]);
